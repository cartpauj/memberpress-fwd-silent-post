<?php
/*
Plugin Name: MemberPress Forward Silent Post
Plugin URI: http://memberpress.com
Description: Allows you to catch Authorize.net Silent Post requests and forward them to multiple URL's. Effectively, allowing you to share the same Authorize.net account with multiple applications.
Version: 1.0.1
Author: Caseproof, LLC
Author URI: http://caseproof.com
Text Domain: memberpress
Copyright: 2004-2013, Caseproof, LLC
*/

if(!defined('ABSPATH')) {die('You are not allowed to call this page directly.');}

include_once(ABSPATH . 'wp-admin/includes/plugin.php');

if(is_plugin_active('memberpress/memberpress.php')) {
  define('MPFWDSP_PLUGIN_SLUG', plugin_basename(__FILE__));
  define('MPFWDSP_PLUGIN_NAME', dirname(MPFWDSP_PLUGIN_SLUG));
  define('MPFWDSP_PATH', WP_PLUGIN_DIR.'/'.MPFWDSP_PLUGIN_NAME);
  define('MPFWDSP_URL', plugins_url('/'.MPFWDSP_PLUGIN_NAME));
  define('MPFWDSP_EDITION', 'memberpress-fwd-silent-post');
  define('MPFWDSP_DBOPS_NAME', 'mpfwdsp_options');
  define('MPFWDSP_DBID_NAME', 'mpfwdsp_id');

  //ACTIONS
  add_action('admin_enqueue_scripts', 'mpfwdsp_enqueue_scripts');
  add_action('mepr_menu', 'mpfwdsp_add_menu_page');
  add_action('admin_init', 'mpfwdsp_maybe_save_options');
  //This one probably should never get hit, but will leave here for testing. wp_ajax_nopriv_ should be used instead.
  add_action('wp_ajax_mpfwdsp_catch_sp', 'mpfwdsp_catch_sp');
  add_action('wp_ajax_nopriv_mpfwdsp_catch_sp', 'mpfwdsp_catch_sp');

  //FILTERS

  //FUNCTIONS
  function mpfwdsp_enqueue_scripts($hook) {
    if(strstr($hook, MPFWDSP_EDITION) !== false) {
      wp_enqueue_script('mpfwdsp_js', MPFWDSP_URL.'/scripts/admin.js', array('jquery-ui-sortable'));
      wp_enqueue_style('mpfwdsp_css', MPFWDSP_URL.'/scripts/admin.css');
    }
  }

  function mpfwdsp_add_menu_page() {
   add_submenu_page('memberpress', __('Forward Silent Post', 'memberpress'), __('Forward Silent Post', 'memberpress'), 'administrator', MPFWDSP_EDITION, 'mpfwdsp_show_menu_page');
  }

  function mpfwdsp_show_menu_page() {
    $urls = get_option(MPFWDSP_DBOPS_NAME, array());
    $unique_id = get_option(MPFWDSP_DBID_NAME, false);

    if($unique_id === false) {
      $unique_id = strtoupper(uniqid());

      update_option(MPFWDSP_DBID_NAME, $unique_id);
    }

    // $hash = strtoupper(substr(md5($unique_id),0,20));

    require(MPFWDSP_PATH.'/views/admin_page.php');
  }

  function mpfwdsp_maybe_save_options() {
    $urls = array();

    if(!isset($_POST['mpfwdsp_save']) || empty($_POST['mpfwdsp_save'])) {
      return;
    }

    if(!empty($_POST['mpfwdsp_url'])) {
      foreach($_POST['mpfwdsp_url'] as $url) {
        if(!empty($url)) {
          $urls[] = (object)array('url' => stripslashes($url));
        }
      }

      update_option(MPFWDSP_DBOPS_NAME, $urls);
      wp_redirect(admin_url('admin.php?page='.MPFWDSP_EDITION.'&saved=true'));
      die();
    }
  }

  function mpfwdsp_catch_sp() {
    $urls = get_option(MPFWDSP_DBOPS_NAME, array());
    $unique_id = get_option(MPFWDSP_DBID_NAME, false);

    if(empty($urls) || !isset($_POST) || empty($_POST)) {
      _e('Nothing to do.', 'memberpress');
      die();
    }

    if($unique_id === false || !isset($_GET['id']) || $_GET['id'] != $unique_id) {
      _e('The id passed was incorrect.', 'memberpress');
      die();
    }

    foreach($urls as $url) {
      wp_remote_post(stripslashes($url->url), array('body' => stripslashes_deep($_POST)));
    }

    _e('Success', 'memberpress');
    die();
  }
} //End if (is plugin active)
