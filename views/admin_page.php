<?php if(!defined('ABSPATH')) {die('You are not allowed to call this page directly.');} ?>

<div class="wrap">
  <div class="icon32"></div>
  <h2><?php _e('Forward Silent Post', 'memberpress'); ?></h2>
  
  <?php if(isset($_GET['saved']) && $_GET['saved'] == 'true'): ?>
    <div id="message" class="updated below-h2">
      <p><?php _e('Your URLs have been saved.', 'memberpress'); ?></p>
    </div>
  <?php endif; ?>
  
  <form action="" method="post">
    <div id="mpfwdsp-urls-area">
      <strong><?php _e('URLs to forward Authorize.net Silent Post requests to:', 'memberpress'); ?></strong><br/><br/>
      <ul id="mpfwdsp-urls-list">
        <?php if(!empty($urls)): ?>
          <?php foreach($urls as $url): ?>
            <li>
              <span class="mpfwdsp_item">
                <label><?php _e('URL', 'memberpress'); ?></label>
                <input type="text" name="mpfwdsp_url[]" value="<?php echo htmlentities(stripslashes($url->url), ENT_QUOTES); ?>" size="50" />
                
                <span class="remove-span">
                  <a href="#" class="mpfwdsp_remove_url" title="<?php _e('Remove URL', 'memberpress'); ?>"><i class="mp-icon mp-icon-cancel-circled mp-16"></i></a>
                </span>
              </span>
            </li>
          <?php endforeach; ?>
        <?php else: ?>
          <li>
            <span class="mpfwdsp_item">
              <label><?php _e('URL', 'memberpress'); ?></label>
              <input type="text" name="mpfwdsp_url[]" value="" size="50" />
              
              <span class="remove-span">
                <a href="#" class="mpfwdsp_remove_url" title="<?php _e('Remove URL', 'memberpress'); ?>"><i class="mp-icon mp-icon-cancel-circled mp-16"></i></a>
              </span>
            </span>
          </li>
        <?php endif; ?>
      </ul>
      
      <span>
        <a href="#" class="mpfwdsp-new-url" title="<?php _e('Add URL', 'memberpress'); ?>"><i class="mp-icon mp-icon-plus-circled mp-24"></i></a>
      </span>
    </div>
    
    <div style="margin-top:15px;">
      <input type="submit" name="mpfwdsp_save" value="<?php _e('Save URLs', 'memberpress'); ?>" class="button" />
    </div>
  </form>
  
  <div style="margin-top:35px;">
    <strong><?php _e('Silent Post to set in your Authorize.net account:', 'memberpress'); ?></strong><br/>
    <span id="mpfwdsp-sp-url"><?php echo admin_url('admin-ajax.php?action=mpfwdsp_catch_sp&id='.$unique_id); ?></span>
  </div>
  
  <!--
  <div style="margin-top:15px;">
    <strong><?php _e('MD5 Hash to set in your Authorize.net account:', 'memberpress'); ?></strong><br/>
    <span id="mpfwdsp-sp-hash"><?php echo $hash; ?></span>
  </div>
  -->
  
  <div style="margin-top:35px;">
    <p><strong><?php _e('Notice:', 'memberpress'); ?></strong> <em><?php _e('Using more than 5 URLs above will significantly increase the chance of a timeout, causing payments to fail to be tracked by MemberPress or any other application you forward IPNs to. If you need to use more than 5, leaving debug emails turned off on each of your sites can help lower the risk of a timeout.', 'memberpress'); ?></em></p>
  </div>
  
</div> <!-- End wrap -->
