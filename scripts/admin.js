(function($) {
  $(document).ready(function() {
    //Add new URL js
    $('a.mpfwdsp-new-url').click(function() {
      var new_url_row = get_new_url_row();
      
      $(new_url_row).hide().appendTo('ul#mpfwdsp-urls-list').fadeIn(500);
      
      return false;
    });
    
    //Remove url row
    $('body').on('click', 'a.mpfwdsp_remove_url', function() {
      var answer = confirm("Are you sure?");
      
      if(answer) {
        $(this).parent().parent().parent().fadeOut(500, function() {
          $(this).remove();
        });
      }
      
      return false;
    });
    
    //Get a url row
    function get_new_url_row() {
      return '<li>\
                <span class="mpfwdsp_item">\
                  <label>URL</label>\
                  <input type="text" name="mpfwdsp_url[]" value="" size="50" />\
                  <span class="remove-span">\
                    <a href="#" class="mpfwdsp_remove_url" title="Remove URL"><i class="mp-icon mp-icon-cancel-circled mp-16"></i></a>\
                  </span>\
                </span>\
              </li>';
    }
  });
})(jQuery);
